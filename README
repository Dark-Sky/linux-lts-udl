# This is a PKGBUILD using UDL's repo that adds additional DVB/DVBT/ATSC
# device support for AARCH64 arm boards. It uses Dan Johansen's config at
# MANJARO-ARM for the current upstream LTS kernel which has support for
# several 64 bit arm boards. Because it is an upstream kernel some of
# these may have limited support with some of these board's onboard
# hardware. The pi CM4 is not in the upstream 5.15 kernel:

pine64, rpi 3a+/3b/3b+/cm3, rpi 4b/pi400, on2-plus, rockpi4b, stationp1,
pine64-lts, nanopc-t4, edgev, pinetab, pinebook, radxa-zero, roc-cc 
vim1-khadas, clockworkpi-a06, on2, gt1-ultimate, pinebook pro, oc4, 
rockpi4c, rockpro64, rock64

# You will need to install a MANJARO-ARM image. I personally like XFCE:

https://manjaro.org/download/#ARM

# Then do a system upgrade with:

sudo pacman -Syu 

# Then install some depends. Take the default by hitting the Enter Key
# on the questions asked during install:

sudo pacman -S --needed base-devel

# Copy and paste all 3 lines as is below in a terminal and hit the Enter Key:

sudo pacman -S xmlto docbook-xsl kmod inetutils bc bison git curl wget \
subversion zvbi patchutils perl-digest-sha1 perl-proc-processtable \
mercurial flex pigz cmake distcc clang meson ninja llvm zstd

# Nothing should be needed config wise with the devices except with the
# Raspberry pi's. They will not boot with the upstream kernel until you
# make cmdline.txt and config.txt changes. Do this before booting into
# the upstream kernel. I have backups in /boot for theses files so I can
# easily switch back and forth between them. Etc: cmdline.txt-manjaro and
# cmdline.txt-upstream / config.txt-manjaro and config.txt-upstream.

# Upstream cmdline.txt. Leave your root=PARTUUID= as is. DO NOT CHANGE!:

root=PARTUUID=????????-?? rw rootwait console=ttyS1,115200 console=tty1 audit=0 snd_bcm2835.enable_headphones=1

# Upstream config.txt. Upstream does not have overlays. Some overlays
# may work other than video ones but you have to make a /boot/overlays
# directory and place the overlay there and enable it in config.txt:

#arm_freq=2000       #If you overclock adjust as needed for your device and enable it

device_tree=dtbs/broadcom/bcm2711-rpi-4-b.dtb    #Must be the right .dtb for your pi device
gpu_mem=512                                      #Adjust as needed for your pi device
initramfs initramfs-linux.img followkernel
kernel=Image
arm_64bit=1
disable_overscan=1

#enable sound
dtparam=audio=on
hdmi_drive=2
enable_uart=1

# Clone this PKGBUILD repo then build/install the kernel:

git clone https://gitlab.com/Dark-Sky/linux-lts-udl.git
cd linux-lts-udl
export MAKEFLAGS="-j4"        #Adjust -j4 to your number of cpu cores
makepkg -si
sudo reboot

# mpv should be installed but vlc is nice to use. A default install will
# not play dvb/atsc streams with out installing some optional depends. 
# I have installed all of the optional depends here I thought I might use;
# some may already installed. Copy and paste all 7 lines as is below in a 
# terminal and hit the Enter Key:

sudo apt-get install vlc avahi aom dav1d libavc1394 libdc1394 flac \
twolame libgme libmtp systemd-libs smbclient libcdio gnu-free-fonts \
ttf-dejavu libssh2 mpg123 lua52-socket libogg libshout libmodplug \
libvpx libvorbis speex opus libtheora libpng libjpeg-turbo \
librsvg x264 x265 zvbi libass libkate libtiger sdl_image srt \
aalib libcaca libpulse alsa-lib libsamplerate libsoxr lirc \
ncurses libnotify gtk3 aribb24 aribb25 pcsclite libdvbpsi

# RPi sound in the past did not work with upstream using pulseaudio.
# I have not checked in a while since I have replaced it with pipewire
# If you have sound/system lock up issues then install pipewire. It works
# well with the RPi kernel also. This install will ask to replace some
# pulseaudio files; just type y to replace them. Copy and paste both 
# lines as is below and hit the Enter Key then reboot:

sudo pacman -S pipewire pipewire-alsa pipewire-media-session \
pipewire-pulse pipewire-v4l2 pipewire-zeroconf

# updateDVB and extra-fta-firmware will get installed with this kernel.
# updateDVB is good for scanning and playing FTA streams using vlc/mpv. 
# extra-fta-firmware provides some extra DVB/DVBT/ATSC firmware not in
# the linux-firmware package.
